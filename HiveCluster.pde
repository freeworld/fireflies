class HiveCluster{
  boolean DEBUG = false; // SHOW DEBUG HELP
  
  ArrayList<Firefly> fireflies = new ArrayList<Firefly>();
  
  // starting position of hive
  float xPos;
  float yPos;
  
  // max range where fireflies can go
  float radius;
  
  float glowingGrowPercentagePerBump = 0.1;
  
  int minNrNewFireflyMovementsUntilReset = 1;
  
  HiveCluster(float xP, float yP, float rad, int nrFireflies){
    xPos = xP;
    yPos = yP;
    radius = rad;
    for (int i = 0; i < nrFireflies; i++){
      float a = random(0, 2*PI);
      // random position in circle, drawing is based on hiveCluster
      float x = random(0,radius) * cos(a);
      float y = random(0,radius) * sin(a);
      Firefly f = new Firefly(x, y);
      fireflies.add(f);
    }
  }
  
  void update(float simulationSpeedMultiplier){
    pushMatrix();
    translate(xPos, yPos);
    
    for (Firefly f : fireflies){
      f.update();
    }
    popMatrix();

    resolveCollision();
    
    resolveOutOfBounds();
  }
  
  void resolveCollision(){
    for (Firefly f1 : fireflies){
      for (Firefly f2 : fireflies){
        if (f1 == f2){ // do not check for collision if same firefly
          continue;
        }
        
        // TODO check for collision
        if (f1.collidesWith(f2)){
          // TODO? resolve collision
          
          // if one of the two is glowing, make the second one glowing as well
          if (f1.glowingPercentage > f2.glowingPercentage){
            // give some percentage to f2 based on f1 glowing level
            f2.glowingPercentage += glowingGrowPercentagePerBump * f1.glowingPercentage;
            if (f2.glowingPercentage > 1){
              f2.glowingPercentage = 1;
            }
          }else if (f2.glowingPercentage > f1.glowingPercentage){
            // give some percentage to f1 based on f2 glowing level
            f2.glowingPercentage += glowingGrowPercentagePerBump * f1.glowingPercentage;
            if (f2.glowingPercentage > 1){
              f2.glowingPercentage = 1;
            }
          }
          
        }
        // otherwise do nothing, these two fireflies do not collide
        
      }
    }
  }
  
  void resolveOutOfBounds(){
    for (Firefly f : fireflies){
      if (f.targetHiveCluster != this){ // only correct firefly position if it is not currently flying to this hive cluster
        if (!isPointInsideHiveRadius(f.xPos, f.yPos) && (!f.hasToReachTargetPosition)){ // check if firefly is outside of our set radius and was not already sent to a position inside our cluster
          // adapt position to be inside the radius
          /*
          float vX = f.xPos;
          float vY = f.yPos;
          float magV = sqrt(vX*vX + vY*vY);
          float aX = (vX / magV) * radius;
          float aY = (vY / magV) * radius;
          f.xPos = aX;
          f.yPos = aY;
          //*/
          
          //// NEW //// with momentum based movement
          // set new direction, as otherwise it might be too apparent that the firefly hit the boundaries.. (fly to random point inside cluster)          
          float a = random(0, 2*PI);
          float x = random(0,radius) * cos(a);
          float y = random(0,radius) * sin(a);
          f.setNextMovDirAndTime(new PVector(x,y)); // set to random point in cluster
        }
      }
    }
  }
  
  boolean isPointInsideHiveRadius(float x, float y){
    return (pow(x, 2) + pow(y, 2) < pow(radius,2));
  }
  
  void addFirefly(Firefly f){
    fireflies.add(f);
  }

  void removeFirefly(Firefly f){
    // only remove if available
    if (fireflies.contains(f)){ // most likely this is unnecessary
      fireflies.remove(f);
    }
  }
  
  void paint(){
    // for fireflies go from hive position
    pushMatrix();
    translate(xPos, yPos);
    
    if (DEBUG){
      stroke(0,0,255);
      fill(0,0,0,0); // transparent fill
      ellipse(0,0, radius, radius);
    }
    

    for (Firefly f : fireflies){
      f.paintGlow();
    }
    
    for (Firefly f : fireflies){
      f.paint();
    }
    
    popMatrix();
  }
  
  int nrOfFireflies(){
    return fireflies.size();
  }
  
}