  ArrayList<HiveCluster> hiveClusters = new ArrayList<HiveCluster>();
  ArrayList<HiveCluster> hiveClustersWithGlowingFireflies = new ArrayList<HiveCluster>();
  ArrayList<HiveCluster> hiveClustersWithoutGlowingFireflies = new ArrayList<HiveCluster>();
  
  float simulationSpeedMultiplier = 1.0;
  
  // starting hivecluster where first single glowing firefly will move to
  // we need a reference to remove it because it should not have any fireflies inside later
  // and not be chosen to give fireflies to another cluster
  HiveCluster pseudoCenterHiveCluster = null;
 
  
  // special entry point for first key down, tracked here
  boolean startedSimulation = false;
  
  void setup() {
    size(1000, 700); 
    initialize();
    frameRate(20);
  }

void initialize() {

  // some settings
  int nrHiveClusters = 9;
  int minNrFirefliesPerCluster = 20;
  int maxNrFirefliesPerCluster = 50;
  float hiveRadiusPerFirefly = 2.5;
  float safetyBorderHiveRadius = 10;
  
  float maximumRadiusToInitHiveClusters = (min(width, height)/2.0) - safetyBorderHiveRadius;
  boolean WidthBiggerThanHeight = false;
  if (width > height){
    WidthBiggerThanHeight = true;
  }
  float freeSpaceRadiusAtScreenCenter = (min(width, height)/4.0);
  
  float safetyMarginRadiantInCircle = (2*PI)/(nrHiveClusters);
  
  // initialize n hive clusters and fill them with fireflies
  // make init positions of hives pseudo-random [ TODO fully determined? ]
  for (int i = 0; i < nrHiveClusters; i++){
    int nrFireflies = ceil(random(minNrFirefliesPerCluster, maxNrFirefliesPerCluster));
    float hiveRadius = nrFireflies * hiveRadiusPerFirefly;
    // generate random position, but make sure the hive's radius is also all inside the window with an additional safety border 
    //float randXPos = random(hiveRadius + safetyBorderHiveRadius,width - hiveRadius - safetyBorderHiveRadius);
    //float randYPos = random(hiveRadius + safetyBorderHiveRadius,height - hiveRadius - safetyBorderHiveRadius);
    // newer smarter positioning in a donut around the center of the screen
    float a = random(((2*PI*(i)) / nrHiveClusters) + safetyMarginRadiantInCircle, ((2*PI*(i+1)) / nrHiveClusters) - safetyMarginRadiantInCircle);
    // random position in circle
    float randXPos = random(freeSpaceRadiusAtScreenCenter,maximumRadiusToInitHiveClusters - hiveRadius) * cos(a) + (width/2.0); // add center to translate
    if (WidthBiggerThanHeight){
      if (random(0,1) > 0.5){ // only do re-adjusting 50% of the time at random
        if (randXPos > (width / 2.0)){ // we are on right side
          randXPos += random(0,(width/2.0) - (height/2.0)); // if there is still space on right side, go a bit to the right at random
        }else if (randXPos < (width / 2.0)){ // we are on left side
          randXPos -= random(0,(width/2.0) - (height/2.0)); // if there is still space on right side, go a bit to the right at random
        }
      }
    }
    float randYPos = random(freeSpaceRadiusAtScreenCenter,maximumRadiusToInitHiveClusters - hiveRadius) * sin(a) + (height/2.0);
    
    HiveCluster h = new HiveCluster(randXPos, randYPos, hiveRadius, nrFireflies);
    // h.fireflies.get(0).glowingPercentage = 1;
    hiveClusters.add(h);
    hiveClustersWithoutGlowingFireflies.add(h);
  }
    
}

void draw() {
  background(0);
  
  // update hives and fireflies
  // and then draw them
  for (HiveCluster h : hiveClusters){
    h.update(simulationSpeedMultiplier);
    h.paint();
  }
  
///////////////////////////////////////////speichert die frames in einen ordner um danach ein video zu machen
  //saveFrame("frames/#####.tga"); //////////auskommentieren um frames zu erzeugen!!!!!
///////////////////////////////////////////
}

void mousePressed() {
  
}

void mouseReleased() {

}

void keyPressed() {
  
  if (key == ' ') { // space key goes further with moving fireflies to next cluster
  
    // special entry point to sudo cluster in the middle of the screen
    if (!startedSimulation){
      // make one single firefly that is in the middle or so and set its target hivecluster to one of the existing ones randomly(?)
      // the rest is magic <3
      
      // out of bounds cluster for initialization, fly-in of first firefly
      HiveCluster outOfBoundsStarterCluster = new HiveCluster (width/2.f, -10,3,0); // init outside of bounds, no fireflies, very small radius
      hiveClusters.add(outOfBoundsStarterCluster);
      
      // create single glowing starter firefly
      // position at center, but calculate in coordinates relative to randomStarterHiveCluster's center
      Firefly starter = new Firefly(0,0);
      starter.glowingPercentage = 1;
      outOfBoundsStarterCluster.addFirefly(starter);
  
      // "hacky way" to make firefly move to center at startup, add new empty hivecluster
      pseudoCenterHiveCluster = new HiveCluster(width/2.f, height/2.f, 30, 0); // init with some radius such that firefly is not stuck
      pseudoCenterHiveCluster.addFirefly(starter);
      outOfBoundsStarterCluster.removeFirefly(starter);
      starter.targetHiveCluster = pseudoCenterHiveCluster;
      starter.xPos += outOfBoundsStarterCluster.xPos - pseudoCenterHiveCluster.xPos;
      starter.yPos += outOfBoundsStarterCluster.yPos - pseudoCenterHiveCluster.yPos;
      
      // add to list of known hiveClusters with glowing fireflies
      hiveClusters.add(pseudoCenterHiveCluster);
      hiveClustersWithGlowingFireflies.add(pseudoCenterHiveCluster);
      
      // done with special init, stop here
      startedSimulation = true;
      return;
    }
    
    // check if there even are clusters without glowing fireflies left
    if (hiveClustersWithGlowingFireflies.size() > 0 && hiveClustersWithoutGlowingFireflies.size() > 0){
      
      // select some fireflies at random which are glowing and send them to another cluster
      // get random hive cluster from the ones with glowing fireflies
      HiveCluster originCluster = hiveClustersWithGlowingFireflies.get(floor(random(hiveClustersWithGlowingFireflies.size())));
      
      // and send off to a hivecluster without glowing fireflies
      HiveCluster targetCluster = hiveClustersWithoutGlowingFireflies.get(floor(random(hiveClustersWithoutGlowingFireflies.size())));
      hiveClustersWithoutGlowingFireflies.remove(targetCluster);
      hiveClustersWithGlowingFireflies.add(targetCluster);
      
      // kill the initial center "hive cluster" for the first single glowing firefly such
      // that it does not mess up the rest of the simulation
      if (pseudoCenterHiveCluster != null && hiveClustersWithGlowingFireflies.contains(pseudoCenterHiveCluster)){
        hiveClustersWithGlowingFireflies.remove(pseudoCenterHiveCluster);
        pseudoCenterHiveCluster = null;
      }
      
      // select a semi-random amount of glowing fireflies to send off
      ArrayList<Firefly> freebirds = new ArrayList<Firefly>();
      int maxNrOfFirefliesToSendOff = floor(random(2,5));
      for (Firefly f : originCluster.fireflies){
        if (freebirds.size() < maxNrOfFirefliesToSendOff){ // check if we need more fireflies
          // only send if glowing enough
          if (f.glowingPercentage >= 0.8){
            freebirds.add(f);
          }
        }else{
          break; // got all the freebirds we need
        }
      }
      
      for (Firefly f : freebirds){
        originCluster.removeFirefly(f); // remove from old cluster
        targetCluster.addFirefly(f); // add to new cluster
        f.targetHiveCluster = targetCluster;
        // set position correctly, switch to new cluster coordinate system
        f.xPos += originCluster.xPos - targetCluster.xPos;
        f.yPos += originCluster.yPos - targetCluster.yPos;
      }
    }
  }else if (key == 'f' || key == 'F'){ // make simulation faster
    simulationSpeedMultiplier += 0.1;
  }else if (key == 's' || key == 'S'){ // make simulation slower
    simulationSpeedMultiplier -= 0.1;
  }else if (key == 'd' || key == 'D'){ // reset simulation speed
    simulationSpeedMultiplier = 1.0;
  }else if (key == 'r' || key == 'R'){ // reset whole simulation
    // TODO
    simulationSpeedMultiplier = 1.0;
  }
}