class Firefly{
  boolean DEBUG = false; // SHOW DEBUG HELP, careful - too many fireflies can slow the simulation down..
  
  color normalColor = color(80, 80, 80);
  color glowingColor = color(204, 153, 0);
  
  HiveCluster targetHiveCluster = null;
  // position relative to hive cluster
  float xPos;
  float yPos;
  float originalMovA = 0;
  float wantedMovA = 0;
  float starttime;
  float expEndTime;
  
  boolean movementInterpolationDirection = false; // false = left, true = right
  boolean leftDirection = false; // lazy, sorry
  boolean rightDirection = true;
  
  float minEndTime = 0.5 * 1000; // in ms
  float maxEndTime = 2.0 * 1000; // in ms
  
  float radius = 3; // radius for drawing circle (firefly representation)
  float glowingPercentage = 0;
  float minMovementSpeed = 0.5;
  float maxMovementSpeed = 1.5;
  
  boolean hasToReachTargetPosition = false;
  PVector targetPosition;
  
  
  Firefly(float x, float y){
    xPos = x;
    yPos = y;
    
    // init mov dir and its duration randomly
    setNextMovDirAndTime();
    setNextMovDirAndTime(); // two times to also set initial movement dir randomly
    
    // init the movDir randomly, as well as the wanted dir and the duration until the next wantedDir change
    //currentMovDir = PVector.random2D();
    // originalMovA = acos(PVector.random2D().x);
    // setNextWantedMovDirAndTime();
  }
  
  // does this collide with another firefly?
  boolean collidesWith(Firefly other){
    if (pow(xPos - other.xPos,2) + pow(yPos - other.yPos,2) <= pow(radius,2)){
      return true;
    }
    return false;
  }
  
    // possible TODO: add some flickering to the firefly by varying glowingColor (select random color from array every few frames, possibly play with transparency)
   color currentColor(){
     return lerpColor(normalColor, glowingColor, glowingPercentage);
   }

  public void setNextMovDirAndTime(){
    starttime = millis();
    expEndTime = starttime + random(minEndTime, maxEndTime);
    originalMovA = wantedMovA; // set to the one the firefly just reached (possibly store current to set it here ultracorrectly)
    
    if (random(0,1) > 0.5){ // choose movement direction randomly
      movementInterpolationDirection = true;
    }else{
      movementInterpolationDirection = false;
    }
    
    /* // old attempt which was not working correctly, now moving directly to target in update
    // if need to reach a target position, we do not set a new wantedMovA randomly, but keep the old one
    if (hasToReachTargetPosition && (!isNearTargetPosition())){
      return;
    }else if (hasToReachTargetPosition && isNearTargetPosition()){
      // reached target position if had to
      hasToReachTargetPosition = false;
    }//*/
    wantedMovA = acos(PVector.random2D().x); // set wanted to new random

  }

  public void setNextMovDirAndTime(PVector targetPoint){
    starttime = millis();
    expEndTime = starttime + random(minEndTime, maxEndTime);
    originalMovA = wantedMovA; // set to the one the firefly just reached (possibly store current to set it here ultracorrectly)
    PVector directionToPoint = new PVector(targetPoint.x - xPos, targetPoint.y - yPos);
    directionToPoint.normalize();
    wantedMovA = acos(directionToPoint.x); // set wanted to new random
    if (random(0,1) > 0.5){ // choose movement direction randomly
      movementInterpolationDirection = true;
    }else{
      movementInterpolationDirection = false;
    }
    
    targetPosition = targetPoint;
    hasToReachTargetPosition = true;
  }
  
  void update(){
    
    if (targetHiveCluster != null){
      // move to hive cluster
      // calc direction the firefly has to move
      // cluster center always at 0,0 because position of firefly relative to cluster
      float hiveClusterCenterX = 0;
      float hiveClusterCenterY = 0;
      
      float xDir = (hiveClusterCenterX - xPos);
      float yDir = (hiveClusterCenterY - yPos);
      
      float directionVectorLength = sqrt((xDir * xDir) + (yDir * yDir));
      
      // normalize for direction vector
      xDir = xDir / directionVectorLength;
      yDir = yDir / directionVectorLength;
      
      xPos += maxMovementSpeed * xDir * simulationSpeedMultiplier; // move in direction of target hive
      //xPos += random(-maxMovementSpeed/1.5, maxMovementSpeed/1.5); // add random jitter
      yPos += maxMovementSpeed * yDir * simulationSpeedMultiplier; // move in direction of target hive
      //yPos += random(-maxMovementSpeed/1.5, maxMovementSpeed/1.5); // add random jitter
      
      if (isNearHiveClusterCenter()){
        // reached target hive cluster
        targetHiveCluster = null;
      }
      
    }else{
      
      if (hasToReachTargetPosition){
        float xDir = (targetPosition.x - xPos);
        float yDir = (targetPosition.y - yPos);
        
        float directionVectorLength = sqrt((xDir * xDir) + (yDir * yDir));
        
        // normalize for direction vector
        xDir = xDir / directionVectorLength;
        yDir = yDir / directionVectorLength;
        
        xPos += maxMovementSpeed * xDir * simulationSpeedMultiplier; // move in direction of target hive
        //xPos += random(-maxMovementSpeed/1.5, maxMovementSpeed/1.5); // add random jitter
        yPos += maxMovementSpeed * yDir * simulationSpeedMultiplier; // move in direction of target hive
        //yPos += random(-maxMovementSpeed/1.5, maxMovementSpeed/1.5); // add random jitter
        
        if (isNearTargetPosition()){
          // reached target position
          hasToReachTargetPosition = false;
        }
        
        return;
      }
      
      
      //// NEW ////
      float curtime = millis();
      
      if (expEndTime <= curtime){
        setNextMovDirAndTime();
        if (DEBUG){
          print("--Setting next mov dir and time--\n");
        }
      }

      float differenceA = abs(originalMovA - wantedMovA);

      if (movementInterpolationDirection == rightDirection){
        if (wantedMovA > originalMovA){
          differenceA = (2 * PI) - differenceA;
        }
      }
      if (movementInterpolationDirection == leftDirection){
        if (originalMovA > wantedMovA){
          differenceA = (2 * PI) - differenceA;
        }        
      }

      float curA = lerp(0, differenceA, (curtime - starttime) / (expEndTime - starttime));
      
      float finalA = 0;

      if (movementInterpolationDirection == leftDirection){
        // left dir
        finalA = originalMovA + curA;
      }else{
        // right dir
        finalA = originalMovA - curA;
      }
  
      if (DEBUG){
        print("<" + originalMovA + "> - <" + wantedMovA + "> - <" + differenceA + "> - <" + finalA + ">\n");
      }
  
      float randomSpeedMultiplier = maxMovementSpeed;//random(minMovementSpeed,maxMovementSpeed);
      PVector posChange = new PVector(cos(finalA), sin(finalA));
      posChange.normalize();
      
      xPos += posChange.x * (simulationSpeedMultiplier * randomSpeedMultiplier);
      yPos += posChange.y * (simulationSpeedMultiplier * randomSpeedMultiplier);
      
      if (DEBUG){
        // calculate wanted and original moving direction vectors
        PVector originalMovDir = new PVector(cos(originalMovA), sin(originalMovA));
        PVector wantedMovDir = new PVector(cos(wantedMovA), sin(wantedMovA));
        
        // DEBUG visual
        pushMatrix();
        stroke(color(255,255,0));
        line(xPos, yPos, xPos + posChange.x * 15, yPos + posChange.y * 15);
        stroke(color(0,255,0));
        line(xPos, yPos, xPos + wantedMovDir.x * 15, yPos + wantedMovDir.y * 15);
        stroke(color(255,0,0));
        line(xPos, yPos, xPos + originalMovDir.x * 15, yPos + originalMovDir.y * 15);
        popMatrix();
      }
    }
    
  }
  
  boolean isNearHiveClusterCenter(){
    float tolerance = radius * 3; // some tolerance in which the firefly is considered to be near the hive cluster center
    // cluster center is always at 0,0, because firefly position is relative to cluster center
    
    boolean xPosNear = (xPos < tolerance) && (xPos > -tolerance);
    boolean yPosNear = (yPos < tolerance) && (yPos > -tolerance);
    
    return (xPosNear && yPosNear);
  }
  
  boolean isNearTargetPosition(){
    float tolerance = radius * 3; // some tolerance in which the firefly is considered to be near the hive cluster center
    
    boolean xPosNear = (abs(xPos-targetPosition.x) < tolerance);
    boolean yPosNear = (abs(yPos-targetPosition.y) < tolerance);
    
    return (xPosNear && yPosNear);
  }
  
  void paintGlow(){
    if (glowingPercentage > 0){
      pushMatrix();
      fill(color(currentColor(), 25));
      stroke(color(currentColor(), 25)); // stroke same color as glow
      translate(xPos, yPos);
      ellipse(0, 0, radius*15, radius*15);
      ellipse(0, 0, radius*10, radius*10);
      ellipse(0, 0, radius*5, radius*5);
      
      popMatrix();
    }
  }
  
  void paint(){
    pushMatrix();
    fill(currentColor());
    stroke(currentColor());
    translate(xPos, yPos);
    ellipse(0, 0, radius*2, radius*2);
    popMatrix();
  }
  
}